/// @file
/// @brief Main application file

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define GENERAL_OFFSET 0x3c

/// @brief COFF File Header has a fixed size - 20 bytes.
struct __attribute__((packed)) PE_Headers
{
  /// @brief Contains PE\0\0; indicates the beginning of the main body of the header.
  uint32_t magic;
  /// @brief The number that identifies the type of target machine.
  uint16_t machine;
  /// @brief The number of sections. This indicates the size of the section table, which immediately follows the headers.
  uint16_t number_of_sections;
  /// @brief The low 32 bits of the number of seconds since 00:00 January 1, 1970 (a C run-time time_t value), which indicates when the file was created.
  uint32_t time_date_stamp;
  /// @brief The file offset of the COFF symbol table, or zero if no COFF symbol table is present. This value should be zero for an image because COFF debugging information is deprecated.
  uint32_t pointer_to_symbol_table;
  /// @brief The number of entries in the symbol table. This data can be used to locate the string table, which immediately follows the symbol table. This value should be zero for an image because COFF debugging information is deprecated.
  uint32_t number_of_symbols;
  /// @brief The size of the optional header, which is required for executable files but not for object files. This value should be zero for an object file.
  uint16_t size_of_optional_header;
  /// @brief The flags that indicate the attributes of the file.
  uint16_t characteristics;
};

struct __attribute__((packed)) PE_Optional_Headers
{
  /// @brief The unsigned integer that identifies the state of the image file. The most common number is 0x10B, which identifies it as a normal executable file. 0x107 identifies it as a ROM image, and 0x20B identifies it as a PE32+ executable.
  uint16_t magic;
  /// @brief The linker major version number.
  uint8_t major_linker_version;
  /// @brief The linker minor version number.
  uint8_t minor_linker_version;
  /// @brief The size of the code (text) section, or the sum of all code sections if there are multiple sections.
  uint32_t size_of_code;
  /// @brief The size of the initialized data section, or the sum of all such sections if there are multiple data sections.
  uint32_t size_of_initialized_data;
  /// @brief The size of the uninitialized data section (BSS), or the sum of all such sections if there are multiple BSS sections.
  uint32_t size_of_uninitialized_data;
  /// @brief The address of the entry point relative to the image base when the executable file is loaded into memory. For program images, this is the starting address. For device drivers, this is the address of the initialization function. An entry point is optional for DLLs. When no entry point is present, this field must be zero.
  uint32_t address_of_entry_point;
  /// @brief The address that is relative to the image base of the beginning-of-code section when it is loaded into memory.
  uint32_t base_of_code;
  /// @brief The address that is relative to the image base of the beginning-of-data section when it is loaded into memory.
  uint32_t base_of_data;
  /// @brief The preferred address of the first byte of image when loaded into memory; must be a multiple of 64 K. The default for DLLs is 0x10000000. The default for Windows CE EXEs is 0x00010000. The default for Windows NT, Windows 2000, Windows XP, Windows 95, Windows 98, and Windows Me is 0x00400000.
  uint64_t image_base;
  /// @brief The alignment (in bytes) of sections when they are loaded into memory. It must be greater than or equal to FileAlignment. The default is the page size for the architecture.
  uint32_t section_alignment;
  /// @brief The alignment factor (in bytes) that is used to align the raw data of sections in the image file. The value should be a power of 2 between 512 and 64 K, inclusive. The default is 512. If the SectionAlignment is less than the architecture's page size, then FileAlignment must match SectionAlignment.
  uint32_t file_alignment;
  /// @brief The major version number of the required operating system.
  uint16_t major_operating_system_version;
  /// @brief The minor version number of the required operating system.
  uint16_t minor_operating_system_version;
  /// @brief The major version number of the image.
  uint16_t major_image_version;
  /// @brief The minor version number of the image.
  uint16_t minor_image_version;
  /// @brief The major version number of the subsystem.
  uint16_t major_subsystem_version;
  /// @brief The minor version number of the subsystem.
  uint16_t minor_subsystem_version;
  /// @brief Reserved, must be zero.
  uint32_t win32_version_value;
  /// @brief The size (in bytes) of the image, including all headers, as the image is loaded in memory. It must be a multiple of SectionAlignment.
  uint32_t size_of_image;
  /// @brief The combined size of an MS-DOS stub, PE header, and section headers rounded up to a multiple of FileAlignment.
  uint32_t size_of_headers;
  /// @brief The image file checksum. The algorithm for computing the checksum is incorporated into IMAGHELP.DLL. The following are checked for validation at load time: all drivers, any DLL loaded at boot time, and any DLL that is loaded into a critical Windows process.
  uint32_t check_sum;
  /// @brief The subsystem that is required to run this image.
  uint16_t subsystem;
  /// @brief For more information, see DLL Characteristics later in this specification.
  uint16_t dll_characteristics;
  /// @brief The size of the stack to reserve. Only SizeOfStackCommit is committed; the rest is made available one page at a time until the reserve size is reached.
  uint64_t size_of_stack_reserve;
  /// @brief The size of the stack to commit.
  uint64_t size_of_stack_commit;
  /// @brief The size of the local heap space to reserve. Only SizeOfHeapCommit is committed; the rest is made available one page at a time until the reserve size is reached.
  uint64_t size_of_heap_reserve;
  /// @brief The size of the local heap space to commit.
  uint64_t size_of_heap_commit;
  /// @brief Reserved, must be zero.
  uint32_t loader_flags;
  /// @brief The number of data-directory entries in the remainder of the optional header. Each describes a location and size.
  uint32_t number_of_rva_and_sizes;
  /// @brief The export table address and size.
  uint64_t export_table;
  /// @brief The import table address and size.
  uint64_t import_table;
  /// @brief The resource table address and size.
  uint64_t resource_table;
  /// @brief The exception table address and size. For more information.
  uint64_t exception_table;
  /// @brief The attribute certificate table address and size.
  uint64_t certificate_table;
  /// @brief The base relocation table address and size.
  uint64_t base_relocation_table;
  /// @brief The debug data starting address and size.
  uint64_t debug;
  /// @brief Reserved, must be 0
  uint64_t architecture;
  /// @brief The RVA of the value to be stored in the global pointer register. The size member of this structure must be set to zero.
  uint64_t global_ptr;
  /// @brief The thread local storage (TLS) table address and size.
  uint64_t tls_table;
  /// @brief The load configuration table address and size.
  uint64_t load_config_table;
  /// @brief The bound import table address and size.
  uint64_t bound_table;
  /// @brief The import address table address and size.
  uint64_t iat;
  /// @brief The delay import descriptor address and size.
  uint64_t delay_import_descriptor;
  /// @brief The CLR runtime header address and size.
  uint64_t clr_runtime_header;
  /// @brief Reserved, must be zero.
  uint64_t reserved;
};

/// @brief Each row of the section table is, in effect, a section header. This table immediately follows the optional header, if any. This positioning is required because the file header does not contain a direct pointer to the section table. Instead, the location of the section table is determined by calculating the location of the first byte after the headers. Make sure to use the size of the optional header as specified in the file header.
struct __attribute__((packed)) PE_Section_Headers
{
  /// @brief An 8-byte, null-padded UTF-8 encoded string. If the string is exactly 8 characters long, there is no terminating null. For longer names, this field contains a slash (/) that is followed by an ASCII representation of a decimal number that is an offset into the string table. Executable images do not use a string table and do not support section names longer than 8 characters. Long names in object files are truncated if they are emitted to an executable file.
  char name[8];
  /// @brief The total size of the section when loaded into memory. If this value is greater than SizeOfRawData, the section is zero-padded. This field is valid only for executable images and should be set to zero for object files.
  uint32_t virtual_size;
  /// @brief For executable images, the address of the first byte of the section relative to the image base when the section is loaded into memory. For object files, this field is the address of the first byte before relocation is applied; for simplicity, compilers should set this to zero. Otherwise, it is an arbitrary value that is subtracted from offsets during relocation.
  uint32_t virtual_address;
  /// @brief The size of the section (for object files) or the size of the initialized data on disk (for image files). For executable images, this must be a multiple of FileAlignment from the optional header. If this is less than VirtualSize, the remainder of the section is zero-filled. Because the SizeOfRawData field is rounded but the VirtualSize field is not, it is possible for SizeOfRawData to be greater than VirtualSize as well. When a section contains only uninitialized data, this field should be zero.
  uint32_t size_of_raw_data;
  /// @brief The file pointer to the first page of the section within the COFF file. For executable images, this must be a multiple of FileAlignment from the optional header. For object files, the value should be aligned on a 4-byte boundary for best performance. When a section contains only uninitialized data, this field should be zero.
  uint32_t pointer_to_raw_data;
  /// @brief The file pointer to the beginning of relocation entries for the section. This is set to zero for executable images or if there are no relocations.
  uint32_t pointer_to_relocations;
  /// @brief The file pointer to the beginning of line-number entries for the section. This is set to zero if there are no COFF line numbers. This value should be zero for an image because COFF debugging information is deprecated.
  uint32_t pointer_to_linenumbers;
  /// @brief The number of relocation entries for the section. This is set to zero for executable images.
  uint16_t number_of_relocations;
  /// @brief The number of line-number entries for the section. This value should be zero for an image because COFF debugging information is deprecated.
  uint16_t number_of_linenumbers;
  /// @brief The flags that describe the characteristics of the section. For more information, see Section Flags.
  uint32_t characteristics;
};

/// @brief PE file structure concatenating header structures
struct __attribute__((packed)) PE_file
{
  struct PE_Headers headers;
  struct PE_Optional_Headers optional_headers;
  struct PE_Section_Headers *section_headers;
};


int8_t move_data_from_exe(FILE* exe_file, struct PE_file* pe)
{
  fseek(exe_file, GENERAL_OFFSET, SEEK_SET);
  uint32_t headers_offset;
  fread(&headers_offset, sizeof(uint32_t), 1, exe_file);
  fseek(exe_file, headers_offset, SEEK_SET);
  struct PE_Headers headers;
  fread(&headers, sizeof(headers), 1, exe_file);
  uint32_t optional_headers_offset = headers_offset + sizeof(headers);
  fseek(exe_file, optional_headers_offset, SEEK_SET);
  struct PE_Optional_Headers optional_headers;
  fread(&optional_headers, headers.size_of_optional_header, 1, exe_file);
  pe->headers = headers;
  pe->optional_headers = optional_headers;

  struct PE_Section_Headers* section_headers = (struct PE_Section_Headers*)malloc(sizeof(struct PE_Section_Headers) * headers.number_of_sections);

  for (uint32_t number_of_section = 0; number_of_section < headers.number_of_sections; number_of_section++)
  {
    uint32_t section_headers_offset = optional_headers_offset + headers.size_of_optional_header + sizeof(struct PE_Section_Headers) * number_of_section;
    fseek(exe_file, section_headers_offset, SEEK_SET);
    struct PE_Section_Headers section_header;
    fread(&section_header, sizeof(struct PE_Section_Headers), 1, exe_file);
    section_headers[number_of_section] = section_header;
  }
  pe->section_headers = section_headers;
  return 0;
}

int8_t extract_section(char name_of_section[], FILE* in, FILE* out)
{
  struct PE_file pe;
  int8_t check = move_data_from_exe(in, &pe);
  if (check != 0)
    return -1;

  for (int number_of_section = 0; number_of_section < pe.headers.number_of_sections; number_of_section++)
  {
    if (strcmp(pe.section_headers[number_of_section].name, name_of_section) != 0)
      continue;

    uint32_t offset_of_section = pe.section_headers[number_of_section].pointer_to_raw_data;
    uint32_t size_of_section = pe.section_headers[number_of_section].size_of_raw_data;
    fseek(in, offset_of_section, SEEK_SET);
    uint8_t* intermediate = (uint8_t*)malloc(sizeof(uint8_t) * size_of_section);
    if (!intermediate)
    {
      free(pe.section_headers);
      printf("Can't allocate memory\n");
      return -1;
    }
    fread(intermediate, sizeof(uint8_t) * size_of_section, 1, in);
    fwrite(intermediate, sizeof(uint8_t) * size_of_section, 1, out);
    free(pe.section_headers);
    free(intermediate);
    return 0;
  }
  free(pe.section_headers);
  printf("Can't find section\n");
  return -1;
}

/// @brief Application entry point
/// @param[in] argc Number of command line arguments
/// @param[in] argv Command line arguments
/// @return 0 in case of success or error code

int main(int argc, char** argv)
{
    (void)argc;
  FILE* input = fopen(argv[1], "r");
  FILE* output = fopen(argv[3], "w");

  if (!input || !output)
  {
    printf("Can't open file");
    (void)getchar();
    return -1;
  }
  int8_t extraction_result = extract_section(argv[2], input, output);
  if (extraction_result)
  {
    (void)getchar();
    return -1;
  }

  fclose(input);
  fclose(output);

  (void)getchar();
  return 0;
}
